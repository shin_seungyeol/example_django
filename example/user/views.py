from django.shortcuts import render
from .models import User
# Create your views here.
def register(request) :
    users = User.objects.order_by('registered')
    return render(request, 'user/register.html', {'users' : users})
